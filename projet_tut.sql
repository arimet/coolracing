-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 15 Mars 2016 à 05:33
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projet_tut`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(4) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `titre`) VALUES
(1, 'Course à pied'),
(2, 'Natation'),
(3, 'Course de VTT'),
(4, 'Marathon');

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id_event` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) CHARACTER SET utf8 NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `date_inscription` date NOT NULL,
  `nb_place` int(11) NOT NULL,
  `adresse` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `photo` text COLLATE utf8_bin NOT NULL,
  `prix` double NOT NULL,
  `id_organisateur` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `id_ville` int(11) NOT NULL,
  `date_ajout` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_event`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=9 ;

--
-- Contenu de la table `event`
--

INSERT INTO `event` (`id_event`, `intitule`, `date_debut`, `date_fin`, `date_inscription`, `nb_place`, `adresse`, `description`, `photo`, `prix`, `id_organisateur`, `id_categorie`, `id_ville`, `date_ajout`) VALUES
(7, 'Course noitel', '2016-03-14', '2016-03-15', '0000-00-00', 148, '50 rue Chamoine', 'Trop cool la course', 'images/no-image.png', 0, 2, 1, 3, '2016-03-14 14:56:13'),
(8, 'Natation', '2016-03-16', '2016-03-16', '0000-00-00', 12, '50 rue Piscine', 'Natation Cool !', 'images/natation.jpg', 15, 2, 2, 1, '2016-03-15 02:06:19');

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

CREATE TABLE IF NOT EXISTS `niveau` (
  `id_niveau` int(3) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_niveau`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Contenu de la table `niveau`
--

INSERT INTO `niveau` (`id_niveau`, `nom`) VALUES
(1, 'Jeune'),
(2, 'Senior'),
(3, 'Elite'),
(4, 'Espoir');

-- --------------------------------------------------------

--
-- Structure de la table `niveauxevent`
--

CREATE TABLE IF NOT EXISTS `niveauxevent` (
  `id_event` int(11) NOT NULL,
  `id_niveau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `niveauxevent`
--

INSERT INTO `niveauxevent` (`id_event`, `id_niveau`) VALUES
(0, 0),
(47, 4),
(2, 3),
(3, 2),
(4, 3),
(7, 1),
(8, 3),
(8, 4);

-- --------------------------------------------------------

--
-- Structure de la table `organisateur`
--

CREATE TABLE IF NOT EXISTS `organisateur` (
  `id_organisateur` int(4) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `mdp` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_organisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Contenu de la table `organisateur`
--

INSERT INTO `organisateur` (`id_organisateur`, `nom`, `prenom`, `email`, `mdp`) VALUES
(2, 'rimet', 'anthony', 'tp9.rimet@hotmail.fr', '140694'),
(3, 'RIMET', 'Anthony', 'anthony.rimet03@gmail.com', '0000'),
(4, 'RIMET', 'Anthony', 'test@fr.fr', '00'),
(5, 'Rimet', 'Anthony', 'qsd@fsr.fr', '0');

-- --------------------------------------------------------

--
-- Structure de la table `participant`
--

CREATE TABLE IF NOT EXISTS `participant` (
  `id_participant` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prenom` text CHARACTER SET utf8 NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `mdp` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_participant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=22 ;

--
-- Contenu de la table `participant`
--

INSERT INTO `participant` (`id_participant`, `nom`, `prenom`, `email`, `mdp`) VALUES
(1, 'Rimet', 'Anthony', 'tp9.rimet@hotmail.fr', '00'),
(2, 'RIMET', 'Quentin', 'test@test.fr', '140694'),
(3, 'Rimet', 'Sylvain', 'a@a.fr', '10'),
(4, 'qsd', 'wsq', 'sq@sq.fr', '1'),
(5, 'Rimet', 'anthony', 'tp9.rimett@hotmail.fr', '000'),
(6, 'Rimet', 'anthony', 'tp9.rimettdsf@hotmail.fr', '000'),
(7, 'Rimet', 'quentin', 'test@tesqsdt.fr', '000'),
(8, 'qsdqsd', 'qsdxcvx', 'sdfsf@sdf.de', 'qsd'),
(9, 'Rimet', 'Anthony', 'anthony.rimet03@gmail.com', '140694'),
(10, 'sqddsq', 'qsd', 'qsdqsd@s.vd', 'qsd'),
(11, 'qsdqs', 'qsdqsd', 'qsdqsdq@qsd.vqs', 'qsd'),
(12, 'qssdfdqfgcvv', 'cvxnjfghj', 'gghjd@sdfsdf.s', 'sdf'),
(13, 'Rimet', 'quentin', 'test@tesqsdqsdsqsdt.fr', '000'),
(14, 'Rimet', 'quentin', 'test@tesqsdqsdsqsdjgfjgdjfsdt.fr', '000'),
(15, 'qssdfdqfgcvvqsd', 'cvxnjfghj', 'gghjd@sdqsdfsdf.s', 'qsd'),
(16, 'qsdsfgfg', 'dfgdfg', 'dfgdfgdfgdfgdf@d.d', 'qsdfgfd'),
(17, 'qsd', 'xcvc', 'anthony.rimet03@gmail.com', '654'),
(18, 'qsdqcxcv', 'xcvcqsd', 'anthony.rimet03@gmail.com', '654'),
(19, 'sdqsd', 'sdfxc', 'xcv@sd.fr', 'sdfsdf'),
(20, 'erzgdf', 'dfg', 'a@n.fr', '654'),
(21, 'sdfsg', 'cvxbvc', 'cvbcvb@sqsd.fr', 'sdf');

-- --------------------------------------------------------

--
-- Structure de la table `participe`
--

CREATE TABLE IF NOT EXISTS `participe` (
  `id_event` int(11) NOT NULL,
  `id_participant` int(11) NOT NULL,
  `id_niveau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `participe`
--

INSERT INTO `participe` (`id_event`, `id_participant`, `id_niveau`) VALUES
(1, 1, 0),
(1, 2, 0),
(1, 4, 0),
(7, 8, 0),
(7, 9, 0),
(7, 10, 0),
(7, 11, 0),
(7, 12, 0),
(7, 13, 0),
(7, 14, 0),
(7, 15, 0),
(7, 16, 0),
(7, 17, 0),
(7, 18, 0),
(7, 19, 0),
(7, 20, 0),
(7, 21, 0),
(7, 6, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `id_ville` int(11) NOT NULL AUTO_INCREMENT,
  `ville` varchar(255) CHARACTER SET utf8 NOT NULL,
  `departement` varchar(255) CHARACTER SET utf8 NOT NULL,
  `code_postal` int(5) NOT NULL,
  PRIMARY KEY (`id_ville`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`id_ville`, `ville`, `departement`, `code_postal`) VALUES
(1, 'Nancy', 'Meurthe-et-Moselle', 54000),
(2, 'Metz', 'Moselle', 57000),
(3, 'Commercy', 'Meuse', 55200),
(4, 'Euville', 'Meuse', 55200);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
