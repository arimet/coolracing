<?php

use app\model\Event as Event;
use app\model\Niveau as Niveau;
use app\model\Categorie as Categorie;



$selectCategorie = Categorie::generateSelectAdd();
$selectNiveau = Niveau::generateSelectAdd();
$data= array();

if (isset($_POST['motcle'])){
  $data["motcle"] = $_POST['motcle'];
}

if (isset($_POST['sport']) && $_POST['sport'] != "Indifférent"){
  $data["categorie"] = $_POST['sport'];
}

if (isset($_POST['niveau']) && $_POST['niveau'] != "Indifférent"){
  $data["niveaux"] = $_POST['niveau'];
}
if (isset($_POST['ville']) && strlen($_POST['ville']) >= 1){
  $data["ville"] = $_POST['ville'];
}

if (isset($_POST['prix'])){
  $data["prix"] = $_POST['prix'];
}

if (isset($_POST['max-participant']) && strlen($_POST['max-participant']) >= 1 ){
  $data["max-participant"] = $_POST['max-participant'];
}

$url = 'http://localhost/coolracing/api/rechercheEvent';

$data_string = json_encode($data);

$result = file_get_contents($url, null, stream_context_create(array(
  'http' => array(
    'method' => 'POST',
    'header' => 'Content-Type: application/json' . "\r\n"
    . 'Content-Length: ' . strlen($data_string) . "\r\n",
    'content' => $data_string,
  ),
)));
$resultat= (json_decode($result, true));



$app->render('recherche.twig', array("url" => $uri,
"resultat" => $resultat, "selectCategorie" => $selectCategorie, "selectNiveau" => $selectNiveau));
