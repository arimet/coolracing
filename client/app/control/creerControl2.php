<?php

use app\model\Niveau as Niveau;
use app\model\Ville as Ville;
use app\model\Event as Event;
use app\model\NiveauEvent as NiveauEvent;

//ID ORGANISATEUR
$data= array();
$data['email']=$_SESSION['mail_organisateur'];
$data['pwd']=$_SESSION['mdp_organisateur'];


//ON RECUPERE LE NOM DE L EVENT
if (strlen($_POST['nom_event']) != 0){
  $data['nom_event'] = $_POST['nom_event'];
}

//Recupere la Categorie
if (isset($_POST['categorie'])){
  $data['cat_event']= $_POST['categorie'];
}

//Recupere date debut
if (isset($_POST['date_debut'])){
  $data['date_debut'] = $_POST['date_debut'];
}

//Recupere date fin
if (isset($_POST['date_fin'])){
  $data['date_fin']= $_POST['date_fin'];
}

//Recupere nb de place
if (isset($_POST['place'])){
  $data['nb_place']= $_POST['place'];
}

//Recupere adresse
if (strlen($_POST['adr_event']) != 0){
  $data['adr_event'] = $_POST['adr_event'];
}

//Recupere departement
if (strlen($_POST['dep_event']) != 0){
  $data['nom_dep'] = $_POST['dep_event'];
}

//Recupere CP
if (strlen($_POST['cp_event']) != 0){
  $data['cp'] = $_POST['cp_event'];
}

//Recupere Ville
if (strlen($_POST['ville_event']) != 0){
  $data['nom_ville'] = $_POST['ville_event'];
}

//Traitement du prix
if (strlen($_POST['prix_event']) != 0){
  $data['prix_event']= $_POST['prix_event'];
}else{
  $data['prix_event'] = 0;
}

//Traitement description
if (strlen($_POST['desc_event']) != 0){
  $data['desc_event']= $_POST['desc_event'];
}else{
  $data['desc_event'] = '';
}

if(isset($_POST['niv1'])){
  $data['niveau1'] = "true";
}

if(isset($_POST['niv2'])){
  $data['niveau2'] = "true";
}

if(isset($_POST['niv3'])){
  $data['niveau3'] = "true";
}

if(isset($_POST['niv4'])){
  $data['niveau4'] = "true";
}



if(isset($_FILES['img']) && strlen($_FILES['img']['name'])>1){
  $dossier = './public/images/';
  $fichier = basename($_FILES['img']['name']);
  $taille = filesize($_FILES['img']['tmp_name']);
  $extensions = array('.png', '.gif', '.jpg','.JPG','.jpeg');
  $extension = strrchr($_FILES['img']['name'], '.');
  //Début des vérifications de sécurité...
  if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
  {

  }

  if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
  {
    //On formate le nom du fichier ici...
    $fichier = strtr($fichier,
    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
    // if(move_uploaded_file($_FILES['img']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
    // {
    // }
  }
  $data['photo_event']='/coolracing/client/public/images/'.$fichier;
}else{
  $fichier="no-image.png";
  $data['photo_event']='/coolracing/client/public/images/'.$fichier;
}



$url = 'http://localhost/coolracing/api/creaEvent';
$data_string = json_encode($data);

$result = file_get_contents($url, null, stream_context_create(array(
  'http' => array(
    'method' => 'POST',
    'header' => 'Content-Type: application/json' . "\r\n"
    . 'Content-Length: ' . strlen($data_string) . "\r\n",
    'content' => $data_string,
  ),
)));
$result= (json_decode($result, true));
var_dump($result);
if($result['msg'] == "ok"){
  if(move_uploaded_file($_FILES['img']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
  {
  }
  $app->redirect('/coolracing/client/event/'.$result['key']);
}else{
  $app->redirect('/coolracing/client/creer');
}
?>
