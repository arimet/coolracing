<?php

$result = file_get_contents("http://localhost/coolracing/api/nbPlaceEvent/".$number);
$nb_place= (json_decode($result, true));

$result = file_get_contents("http://localhost/coolracing/api/event/".$number);
$event= (json_decode($result, true));

if(get_http_response_code("http://localhost/api/coolracing/nbInscritEvent/".$number) != "200"){
  $nb_participe = 0;
}else{
  $result = file_get_contents("http://localhost/coolracing/api/nbInscritEvent/".$number);
  $nb_participe= (json_decode($result, true));
}


$plein="false";
$existcompte ="false";
$inscriptionReussi = "false";
$participedeja = "false";
$participe="";
$participant="";
$nonecompte="false";
if($nb_place <= $nb_participe){
  $plein="true";
}

if(isset($_POST['form1'])){
  if (isset($_POST['mail'])){
    $mail = $_POST['mail'];
  }
  if (isset($_POST['password'])){
    $pwd = $_POST['password'];
  }
  if (isset($_POST['nom'])){
    $nom = $_POST['nom'];
  }
  if (isset($_POST['prenom'])){
    $prenom = $_POST['prenom'];
  }
  $url = 'http://localhost/coolracing/api/inscriptionEvent/'.$number;
  $data = array(
    'mail' => $mail,
    'prenom' => $prenom,
    'nom' => $nom,
    'pwd' => $pwd,
    'form' => 1,
  );
  $data_string = json_encode($data);

  $result = file_get_contents($url, null, stream_context_create(array(
    'http' => array(
      'method' => 'POST',
      'header' => 'Content-Type: application/json' . "\r\n"
      . 'Content-Length: ' . strlen($data_string) . "\r\n",
      'content' => $data_string,
    ),
  )));
  $result= (json_decode($result, true));
  if($result['msg'] == "ok"){
    $inscriptionReussi = "true";
  }else{
    $existcompte = "true";
  }
  $participant = $result['participant'];

}elseif (isset($_POST['form2'])) {
  if (isset($_POST['mail'])){
    $mail = $_POST['mail'];
  }
  if (isset($_POST['password'])){
    $pwd = $_POST['password'];
  }
  $url = 'http://localhost/coolracing/api/inscriptionEvent/'.$number;
  $data = array(
    'mail' => $mail,
    'pwd' => $pwd,
    'form' => 2,
  );
  $data_string = json_encode($data);
  $result = file_get_contents($url, null, stream_context_create(array(
    'http' => array(
      'method' => 'POST',
      'header' => 'Content-Type: application/json' . "\r\n"
      . 'Content-Length: ' . strlen($data_string) . "\r\n",
      'content' => $data_string,
    ),
  )));
  $result= (json_decode($result, true));
  if($result['msg'] == "ok"){
    $inscriptionReussi = "true";
    $participant = $result['participant'];
  }elseif ($result['msg'] == "existe") {
    $inscriptionReussi = "true";
    $participedeja = "true";
    $participant = $result['participant'];
  }else{
    $nonecompte="true";
  }

}


$app->render('inscriptionEvent.twig', array("id_event"=> $number,"plein"=>$plein,"existcompte"=>$existcompte,"participedeja"=>$participedeja,
"participe"=>$participe,"participant"=>$participant,"event"=>$event,"inscriptionReussi"=>$inscriptionReussi,"nonecompte"=>$nonecompte));
?>
