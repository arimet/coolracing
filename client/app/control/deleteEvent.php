<?php

use app\model\Participe as Participe;
use app\model\Event as Event;
$event = Event::where("id_event","=",$number)->first();



if(isset($_SESSION['id_organisateur']) && $event['id_organisateur']==$_SESSION['id_organisateur']){
  $deleteEvent = Event::where("id_event","=",$number)->delete();
  $deleteParticipe = Participe::where("id_event","=",$number)->delete();
  $app->render('deleteEvent.twig',array());
}
else{
  $app->redirect('/coolracing/client');
}
?>
