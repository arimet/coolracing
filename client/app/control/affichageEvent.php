<?php

use app\model\Niveau as Niveau;
use app\model\Ville as Ville;
use app\model\Event as Event;
use app\model\Organisateur as Organisateur;
use app\model\NiveauEvent as NiveauEvent;

$result = file_get_contents("http://localhost/coolracing/api/event/".$number);
$event= (json_decode($result, true));

$admin = 'false';
if(isset($_SESSION['id_organisateur']) && $event['id_organisateur'] == $_SESSION['id_organisateur']){
  $admin = 'true';
}
$app->render('event.twig',array("ev" => $event, "admin" => $admin));


 ?>
