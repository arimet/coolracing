<?php

use app\model\Categorie as Categorie;
use app\model\Niveau as Niveau;

if(isset($_SESSION['id_organisateur'])){
  $cat = Categorie::all();
  $niveau = Niveau::all();
$app->render('creer.twig',array('session' => $_SESSION,'cat'=>$cat, 'niveau'=>$niveau));}
else{
  $app->redirect('./');
}
?>
