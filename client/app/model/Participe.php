<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Participe extends Eloquent {

    protected $table = 'participe';
    public $timestamps = false;

    public function event(){
        return $this->belongsTo('app\model\Event','id_event');
    }

    public function participant(){
        return $this->belongsTo('app\model\Participant','id_participant');
    }


}
