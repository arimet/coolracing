<?php

namespace app\model;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Event extends Eloquent {

    protected $table = 'event';
    protected $primaryKey = 'id_event';
    public $timestamps = false;

    public function categorie(){
        return $this->belongsTo('app\model\Categorie','id_categorie');
    }

    public function niveaux(){
        return $this->belongsToMany('app\model\Niveau', 'niveauxevent', 'id_event', 'id_niveau');
    }

    public function ville(){
        return $this->belongsTo('app\model\Ville', 'id_ville');
    }

    public function organisateur(){
        return $this->belongsTo('app\model\Organisateur', 'id_organisateur');
    }

    public function participants(){
        return $this->belongsToMany('app\model\Participant', 'participe', 'id_event', 'id_participant');
    }

}
