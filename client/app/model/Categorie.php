<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Categorie extends Eloquent {


    protected $table = 'categorie';
    protected $primaryKey = 'id_categorie';
    public $timestamps = false;

    public function events(){
        return $this->hasMany('app\model\Event', 'id_categorie');
    }

    public static function generateSelectAdd() {
        return (Categorie::all());
    }

}
