<?php

namespace app\model;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Ville extends Eloquent {

    protected $table = 'ville';
    protected $primaryKey = 'id_ville';
    public $timestamps = false;

    public function events(){
        return $this->hasMany('app\model\Event', 'id_ville');
    }

    public function participants(){
        return $this->hasMany('app\model\Participant', 'id_ville');
    }
}
