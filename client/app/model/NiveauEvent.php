<?php

namespace app\model;
use Illuminate\Database\Eloquent\Model as Eloquent;


class NiveauEvent extends Eloquent {

    protected $table = 'niveauxevent';
    public $timestamps = false;

    public function event(){
        return $this->belongsTo('app\model\Event','id_event');
    }

    public function niveaux(){
        return $this->belongsTo('app\model\Niveau','id_niveau');
    }



}
