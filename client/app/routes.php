<?php

use app\model\Categorie as Categorie;
use app\model\Niveau as Niveau;
use app\model\Event as Event;
use app\model\Ville as Ville;

$req = $app::getInstance()->request();
$uri = $req->getUrl().$req->getRootUri();

function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}


// Affiche la page d'accueil (Appel API)
$app->get('/', function () use ($app) {
  $result = file_get_contents("http://localhost/coolracing/api/listeAccueil");
  $events= (json_decode($result, true));
  $app->render('accueil.twig',array("ev" => $events));
});


// Login
$app->get('/login', function () use ($app){
  if (isset($_SESSION['inscrit'])){
    $app->render('login.twig',array("etat"=>"ok"));
  }else{
    $app->render('login.twig');
  }
});

$app->post('/login', function() use ($app){
  include('control/login.php');
});


// Deconnexion
$app->get('/deco', function() use ($app){
  $_SESSION=array();
  $app->redirect('./');
});

//Inscription
$app->get('/inscription', function () use ($app){
  $app->render('inscription.twig');
});

$app->post('/inscription', function() use ($app){
  include('control/inscription.php');
});

//Lister (Appel API)
$app->get('/liste', function () use ($app) {
  $result = file_get_contents("http://localhost/coolracing/api/liste");
  $events= (json_decode($result, true));
  $app->render('liste.twig',array("ev" => $events));
});



//lister events de l'organisateur
$app->get('/eventPerso/:number', function($number) use ($app) {
  include('control/eventPersoControl.php');

});

//Creer evenement
$app->get('/creer', function () use ($app){
  include('control/creerControl.php');
});

$app->post('/creer', function () use ($app){
  include('control/creerControl2.php');
});

//affichage event (Appel API)
$app->get('/event/:number', function($number) use ($app){
  include('control/affichageEvent.php');
});


//inscriptionEvent (Appel API)
$app->post('/inscriptionEvent/:number', function($number) use ($app){
  include('control/inscriptionEvent.php');
});

//desinscriptionEvent (Appel API)
$app->post('/desinscriptionEvent/:number', function($number) use ($app){
  include('control/desinscriptionEvent.php');
});

//dossardEvent
$app->post('/dossard/:number', function($number) use ($app){
  include('control/dossardEvent.php');
});

//listeInscrit event
$app->post('/inscritEvent/:number', function($number) use ($app){
  include('control/inscritEvent.php');
});

//deletEvent
$app->post('/deleteEvent/:number', function($number) use ($app){
  include('control/deleteEvent.php');
});

// Recherche
$app->get('/recherche', function() use ($app, $uri) {
  $selectCategorie = Categorie::generateSelectAdd();
  $selectNiveau = Niveau::generateSelectAdd();
  $app->render('recherche.twig', array("url" => $uri,
  "selectCategorie" => $selectCategorie, "selectNiveau" => $selectNiveau));

});

$app->post('/recherche', function() use ($app, $uri) {
  include('control/RechercheController.php');
});


// If 404 not found
$app->notFound(function() use ($app, $uri) {

  $app->render('erreur.twig', array("url" => $uri));
});


?>
