<?php
require 'vendor/autoload.php';

session_cache_limiter(false);
session_start();

$app = new \Slim\Slim(array(
  'view' => new \Slim\Views\Twig(),
  'templates.path' => 'app/templates'
));
// modification de l'environnement de slim : ajout des variables de session
$twig = $app->view->getEnvironment();
if (isset($_SESSION['id_organisateur'])){
  $twig->addGlobal('session',$_SESSION);
}
require 'app/routes.php';

$app->run();

?>
