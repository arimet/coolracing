<?php
require 'vendor/autoload.php';

session_cache_limiter(false);
session_start();

$app = new \Slim\Slim(array(
));
// modification de l'environnement de slim : ajout des variables de session

require 'app/routes.php';

$app->run();

?>
