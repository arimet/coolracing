<?php
/**
* @api {get} /liste Liste des évenements
* @apiName getListe
* @apiGroup Event
*
*@apiDescription Retourne la liste compléte des événements
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "id_event": 8,
*       "intitule": "Natation",
*       "date_debut": "2016-03-16",
*       "date_fin": "2016-03-16",
*       "date_inscription": "0000-00-00",
*       "nb_place": 12,
*       "adresse": "50 rue Piscine",
*       "description": "Natation Cool !",
*       "photo": "images/natation.jpg",
*       "prix": 15,
*       "id_organisateur": 2,
*       "id_categorie": 2,
*       "id_ville": 1,
*       "date_ajout": "2016-03-15 03:06:19",
*       "categorie": {
*         "id_categorie": 2,
*         "titre": "Natation"
*       },
*       "ville": {
*          "id_ville": 1,
*          "ville": "Nancy",
*          "departement": "Meurthe-et-Moselle",
*          "code_postal": 54000
*       }
*     }
*/


/**
* @api {get} /listeAccueil Liste 6 derniers événements
* @apiName getListeAccueil
* @apiGroup Event
*
*@apiDescription Retourne la liste des 6 derniers événements (pratique pour une page d'accueil)
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "id_event": 8,
*       "intitule": "Natation",
*       "date_debut": "2016-03-16",
*       "date_fin": "2016-03-16",
*       "date_inscription": "0000-00-00",
*       "nb_place": 12,
*       "adresse": "50 rue Piscine",
*       "description": "Natation Cool !",
*       "photo": "images/natation.jpg",
*       "prix": 15,
*       "id_organisateur": 2,
*       "id_categorie": 2,
*       "id_ville": 1,
*       "date_ajout": "2016-03-15 03:06:19",
*       "categorie": {
*         "id_categorie": 2,
*         "titre": "Natation"
*       },
*       "ville": {
*          "id_ville": 1,
*          "ville": "Nancy",
*          "departement": "Meurthe-et-Moselle",
*          "code_postal": 54000
*       }
*     }
*/

/**
* @api {get} /event/:id Event
* @apiName getEvent
* @apiGroup Event
*
*@apiDescription Retourne un evenement en particulier
*
*
*
*@apiParam {Number} id  id de l'événement.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "id_event": 8,
*       "intitule": "Natation",
*       "date_debut": "2016-03-16",
*       "date_fin": "2016-03-16",
*       "date_inscription": "0000-00-00",
*       "nb_place": 12,
*       "adresse": "50 rue Piscine",
*       "description": "Natation Cool !",
*       "photo": "images/natation.jpg",
*       "prix": 15,
*       "id_organisateur": 2,
*       "id_categorie": 2,
*       "id_ville": 1,
*       "date_ajout": "2016-03-15 03:06:19",
*       "categorie": {
*         "id_categorie": 2,
*         "titre": "Natation"
*       },
*       "ville": {
*          "id_ville": 1,
*          "ville": "Nancy",
*          "departement": "Meurthe-et-Moselle",
*          "code_postal": 54000
*       }
*     }
*/

/**
* @api {get} /inscritEvent/:id Particpant Event
* @apiName getInscritEvent
* @apiGroup Partcipe
*
*@apiDescription Permet de retourner la liste des inscrits à un événement
*
*
*
*@apiParam {Number} id  id de l'événement.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "id_event": 1,
*       "id_participant": 1,
*       "id_niveau": 0,
*       "participant": {
*         "id_participant": 1,
*         "nom": "Rimet",
*         "prenom": "Anthony",
*         "email": "tp9.rimet@hotmail.fr",
*         "mdp": "-----"
*       }
*     },
*/

/**
* @api {get} /nbInscritEvent/:id Nombre Particpant Event
* @apiName getnbInscritEvent
* @apiGroup Partcipe
*
*@apiDescription Permet de retourner le nombre d'inscrits à un événement
*
*
*
*@apiParam {Number} id  id de l'événement.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     14
*/


/**
* @api {get} /nbPlaceEvent/:id Nombre Place Event
* @apiName getPlaceEvent
* @apiGroup Event
*
*@apiDescription Permet de retourner le nombre de place d'un évenements
*
*
*
*@apiParam {Number} id  id de l'événement.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     14
*/


/**
* @api {post} /inscriptionEvent/:id Inscription Event
* @apiName postInscription
* @apiGroup Participant
*@apiParam {Number} id  id de l'événement.
*
*
*@apiDescription Permet de s'inscrire Renvoie un message de confirmation ou non.
*
*
*
*@apiParam {Number} form  1 pour un nouveau participant | 2 pour une personne qui a déjà participé à un événement .
*@apiParam {Text} mail  mail du participant.
*@apiParam {Text} pwd  mot de passe du particpant.
*@apiParam {Text} nom  (optionnel si form 2)nom du participant.
*@apiParam {Text} prenom  (optionnel si form 2) prenom du participant.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "msg": "ok",
*       "participant": {
*         "id_participant": 6,
*         "nom": "Rimet",
*         "prenom": "anthony",
*         "email": "tp9.rimettdsf@hotmail.fr",
*         "mdp": "000"
*       }
*     }
*/

/**
* @api {delete} /desinscriptionEvent/:id Desinscription Event
* @apiName postdesInscription
* @apiGroup Participant
*@apiParam {Number} id  id de l'événement.
*
*
*@apiDescription Permet de se déinscrire à un évent. Renvoie un message de confirmation ou non.
*
*
*
*@apiParam {Text} mail  mail du participant.
*@apiParam {Text} pwd  mot de passe du particpant.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "msg": "desincrit",
*     }
*/


/**
* @api {post} /getDossard/:id Get Dossard
* @apiName postDossard
* @apiGroup Participant
*@apiParam {Number} id  id de l'événement.
*
*
*@apiDescription Permet de recuperer le numéro de dossar. Coupler le numéro avec codebarre.js
*
*
*
*@apiParam {Text} mail  mail du participant.
*@apiParam {Text} pwd  mot de passe du particpant.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "msg": "ok",
*       "dossard" :"9845"
*     }
*/

/**
* @api {post} /rechercheEvent/ Recherche Event
* @apiName postRechercheEvent
* @apiGroup Event
*
*
*@apiDescription Permet de rechercher des événements en fonction de certains paramètres.
*
*
*
*@apiParam {Text} motcle  (optionnel) Mots clés de la recherche.
*@apiParam {Number} categorie  (optionnel) Catégorie de l'évenement : 1 Course à pied | 2 Natation | 3 VTT | 4 Marathon.
*@apiParam {Text} niveau  (optionnel) Niveau souhaité : Jeune | Senior | Elite | Espoir.
*@apiParam {Text} ville  (optionnel) Ville de l'événment.
*@apiParam {Number} max-participant  (optionnel) Nombre maximum de participant.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "id_event": 8,
*       "intitule": "Natation",
*       "date_debut": "2016-03-16",
*       "date_fin": "2016-03-16",
*       "date_inscription": "0000-00-00",
*       "nb_place": 12,
*       "adresse": "50 rue Piscine",
*       "description": "Natation Cool !",
*       "photo": "images/natation.jpg",
*       "prix": 15,
*       "id_organisateur": 2,
*       "id_categorie": 2,
*       "id_ville": 1,
*       "date_ajout": "2016-03-15 03:06:19",
*       "categorie": {
*         "id_categorie": 2,
*         "titre": "Natation"
*       },
*       "ville": {
*          "id_ville": 1,
*          "ville": "Nancy",
*          "departement": "Meurthe-et-Moselle",
*          "code_postal": 54000
*       }
*     }
*/

/**
* @api {post} /creaEvent/ Créer Event
* @apiName creerEvent
* @apiGroup Organisateur
*
*
*@apiDescription Permet de créer des événements en fonction de certains paramètres. Retourne un message et le numéro de l'évent.
*
*
*
*@apiParam {Text} email  email de l'organisateur.
*@apiParam {Number} pwd  mdp de l'organisateur.
*@apiParam {Text} nom_event  Nom de l'event.
*@apiParam {Date} date_debut  Date début évent (YYYY-MM-DD).
*@apiParam {Date} date_fin  Date fin évent (YYYY-MM-DD).
*@apiParam {Number} nb_place  Nombre de place évent.
*@apiParam {Text} adr_event  adresse de l'event.
*@apiParam {Text} nom_ville  Nom ville de l'event.
*@apiParam {Text} nom_dep  Nom département de l'event.
*@apiParam {Number} cp  Code postal de l'évent.
*@apiParam {Number} cat_event  Catégorie de l'évenement : 1 Course à pied | 2 Natation | 3 VTT | 4 Marathon.
*@apiParam {Text} photo_event  photo de l'event.
*@apiParam {Number} prix_event  prix de l'évent.
*@apiParam {Text} desc_event  Description de l'event.
*@apiParam {boolean} niveau1  (optionnel) true|false  Jeune.
*@apiParam {boolean} niveau2  (optionnel) true|false  Senior.
*@apiParam {boolean} niveau3  (optionnel) true|false  Elite.
*@apiParam {boolean} niveau4  (optionnel) true|false  Espoir.
*@apiParam {Text} ville  (optionnel) Ville de l'événment.
*@apiParam {Number} max-participant  (optionnel) Nombre maximum de participant.
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "msg": ok,
*       "key": 17,
*     }
*/


?>
