<?php

namespace app\model;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Organisateur extends Eloquent {

    protected $table = 'organisateur';
    protected $primaryKey = 'id_organisateur';
    public $timestamps = false;

    public function events(){
        return $this->hasMany('app\models\Event', 'id_organisateur');
    }
}
