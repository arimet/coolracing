<?php

namespace app\model;
use Illuminate\Database\Eloquent\Model as Eloquent;


class Niveau extends Eloquent {

    protected $table = 'niveau';
    protected $primaryKey = 'id_niveau';
    public $timestamps = false;

    public function events(){
        return $this->belongsToMany('app\models\Event', 'niveauxevent', 'id_niveau', 'id_event');
    }

    public static function generateSelectAdd() {
        return (Niveau::all());
    }

}
