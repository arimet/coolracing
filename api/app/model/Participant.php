<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Participant extends Eloquent {

    protected $table = 'participant';
    protected $primaryKey = 'id_participant';
    public $timestamps = false;


}
