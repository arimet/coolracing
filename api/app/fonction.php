<?php

use app\model\Categorie as Categorie;
use app\model\Niveau as Niveau;
use app\model\Event as Event;
use app\model\Ville as Ville;
use app\model\Participant as Participant;
use app\model\Participe as Participe;
use app\model\Organisateur as Organisateur;
use app\model\NiveauEvent as NiveauEvent;

function liste($app){
  $events = Event::with('categorie','ville')->orderBy('id_event','desc')->get();
  if(strlen($events)>3) {
    $app->response->setStatus(200);
    $app->response()->headers->set('Content-Type', 'application/json');
    echo json_encode($events);
  } else {
    $app->response->setStatus(500);
  }
};

function listeAccueil($app){
  $events = Event::with('ville','categorie')->orderBy('id_event', 'desc')->take(6)->get();
  echo json_encode($events);
};


function inscritEvent($app,$number){
  $participe = Participe::with('participant')->where('id_event','=',$number)->get();
  if(strlen($participe)>3) {
    $app->response->setStatus(200);
    $app->response()->headers->set('Content-Type', 'application/json');
    echo json_encode($participe);
  } else {
    $app->response->setStatus(500);
  }
};

function nbInscritEvent($app,$number){
  $nbParticipe = Participe::with('participant')->where('id_event','=',$number)->count();
  if($nbParticipe) {
    $app->response->setStatus(200);
    $app->response()->headers->set('Content-Type', 'application/json');
    echo json_encode($nbParticipe);
  } else {
    $app->response->setStatus(500);
  }
};

function nbPlaceEvent($app,$number){
  $nbPlace = Event::where('id_event','=',$number)->select('nb_place')->first();
  if($nbPlace) {
    $app->response->setStatus(200);
    $app->response()->headers->set('Content-Type', 'application/json');
    echo json_encode($nbPlace);
  } else {
    $app->response->setStatus(500);
  }
};

function event($app,$number){
  $event = Event::with('ville','organisateur','categorie','niveaux')->where('id_event','=',$number)->first();
  if($event) {
    $app->response->setStatus(200);
    $app->response()->headers->set('Content-Type', 'application/json');
    echo json_encode($event);
  } else {
    $app->response->setStatus(500);
  }
};

function inscriptionEvent($app,$number){
  $json = $app->request->getBody();
  $data = json_decode($json, true);
  if($data['form'] == 1){
    $participant = Participant::where('email','LIKE',$data['mail'])->where('prenom','LIKE',$data['prenom'])->count();
    if($participant ==0){
      $participant = new Participant;
      $participant->nom = $data['nom'];
      $participant->prenom = $data['prenom'];
      $participant->email = $data['mail'];
      $participant->mdp = $data['pwd'];
      $participant->save();
      $participant = Participant::where('email','LIKE',$data['mail'])->where('prenom','LIKE',$data['prenom'])->first();
      $participe = new Participe;
      $participe->id_event= $number;
      $participe->id_participant = $participant['id_participant'];
      $participe->save();
      $participe = Participe::where('id_event','=',$number)->where('id_participant','=',$participant['id_participant'])->first();$app->response->setStatus(200);
      $app->response->setStatus(200);
      $app->response()->headers->set('Content-Type', 'application/json');
      $res = array("msg" =>"ok","participant" => $participant);
      echo json_encode($res);
    }else{
      $app->response->setStatus(301);
      $app->response()->headers->set('Content-Type', 'application/json');
      $res = array("msg" =>"existe","participant" => $participant);
      echo json_encode($res);
    }
  }elseif ($data['form'] == 2) {
    $participant = Participant::where('email','LIKE',$data['mail'])->where('mdp','LIKE',$data['pwd'])->count();
    if($participant == 1){
      $participant = Participant::where('email','LIKE',$data['mail'])->where('mdp','LIKE',$data['pwd'])->first();
      $participe = Participe::where('id_event','=',$number)->where('id_participant','=',$participant['id_participant'])->count();
      if($participe == 0){
        $participe = new Participe;
        $participe->id_event= $number;
        $participe->id_participant = $participant['id_participant'];
        $participe->save();
        $participe = Participe::where('id_event','=',$number)->where('id_participant','=',$participant['id_participant'])->first();
        $app->response->setStatus(200);
        $app->response()->headers->set('Content-Type', 'application/json');
        $res = array("msg" =>"ok","participant" => $participant);
        echo json_encode($res);
      }else{
        $app->response->setStatus(301);
        $app->response()->headers->set('Content-Type', 'application/json');
        $res = array("msg" =>"existe","participant" => $participant);
        echo json_encode($res);
      }
    }else{
      $app->response->setStatus(200);
      $app->response()->headers->set('Content-Type', 'application/json');
      $res = array("msg" =>"noexist");
      echo json_encode($res);
    }
  }
};


function desinscription($app,$number){
  $json = $app->request->getBody();
  $data = json_decode($json, true);
  $participant = Participant::where('email','LIKE',$data['mail'])->where('mdp','LIKE',$data['pwd'])->count();
  if($participant ==1){
    $participant = Participant::where('email','LIKE',$data['mail'])->where('mdp','LIKE',$data['pwd'])->first();
    $participe = Participe::where('id_event','=',$number)->where('id_participant','=',$participant['id_participant'])->count();
    if($participe == 1){
      $participe = Participe::where('id_event','=',$number)->where('id_participant','=',$participant['id_participant'])->delete();
      $app->response()->headers->set('Content-Type', 'application/json');
      $res = array("msg" =>"desinscrit");
      echo json_encode($res);
    }else{
      $app->response()->headers->set('Content-Type', 'application/json');
      $res = array("msg" =>"pasinscrit");
      echo json_encode($res);
    }
  }else{
    $app->response()->headers->set('Content-Type', 'application/json');
    $res = array("msg" =>"noexist");
    echo json_encode($res);
  }
};

function getdossard($app,$number){
  $json = $app->request->getBody();
  $data = json_decode($json, true);
  $participant = Participant::where('email','LIKE',$data['mail'])->where('mdp','LIKE',$data['pwd'])->count();
  if($participant ==1){
    $participant = Participant::where('email','LIKE',$data['mail'])->where('mdp','LIKE',$data['pwd'])->first();
    $participe = Participe::where('id_event','=',$number)->where('id_participant','=',$participant['id_participant'])->count();
    if($participe == 1){
      $app->response()->headers->set('Content-Type', 'application/json');
      $dossard = $number.$participant['id_participant'];
      $res = array("msg" =>"ok","dossard"=>$dossard);
      echo json_encode($res);
    }else{
      $app->response()->headers->set('Content-Type', 'application/json');
      $res = array("msg" =>"pasinscrit");
      echo json_encode($res);
    }
  }else{
    $app->response()->headers->set('Content-Type', 'application/json');
    $res = array("msg" =>"noexist");
    echo json_encode($res);
  }
};

function rechercheEvent($app){
  $json = $app->request->getBody();
  $data = json_decode($json, true);
  $recherche = Event::with('niveaux','ville')->where(function ($query)  use ($data) {
    $query->where("intitule", "like", '%'.$data['motcle'].'%')
    ->orwhere("description", "like", "%".$data['motcle']."%")
    ->orwhere("adresse", "like", "%".$data['motcle']."%");
  });

  //par categorie
  if (isset($data['categorie']) && $data['categorie'] != 0){
    $recherche = $recherche->where(function ($query)  use ($data) {
      $query->where("id_categorie", "=", $data['categorie']);
    });
  }
  //Par nombre max de participant
  if (isset($data['max-participant']) && $data['max-participant'] != 0){
    $recherche = $recherche->where(function ($query) use ($data) {
      $query->where("nb_place","<=",$data['max-participant']);
    });
  }
  //Par niveaux
  if (isset($data['niveaux']) && $data['niveaux'] == "Jeune"){
    $recherche = $recherche->whereHas('niveaux',function ($query) use ($data) {
      $query->where("nom","LIKE",$data['niveaux']);
    });
  }
  if (isset($data['niveaux']) && $data['niveaux'] == "Senior"){
    $recherche = $recherche->whereHas('niveaux',function ($query) use ($data) {
      $query->where("nom","LIKE",$data['niveaux']);
    });
  }
  if (isset($data['niveaux']) && $data['niveaux'] == "Elite"){
    $recherche = $recherche->whereHas('niveaux',function ($query) use ($data) {
      $query->where("nom","LIKE",$data['niveaux']);
    });
  }
  if (isset($data['niveaux']) && $data['niveaux'] == "Espoir"){
    $recherche = $recherche->whereHas('niveaux',function ($query) use ($data) {
      $query->where("nom","LIKE",$data['niveaux']);
    });
  }

  //Par ville
  if (isset($data['ville'])){
    $recherche = $recherche->whereHas('ville',function ($query) use ($data) {
      $query->where("ville","LIKE",$data['ville']);
    });
  }

  //Par prix
  if (isset($data['prix']) && $data['prix'] == "Indifferent"){
    $recherche = $recherche->where("prix", ">=", "0");
  }
  if (isset($data['prix']) && $data['prix'] == "Gratuit"){
    $recherche = $recherche->where("prix", "=", 0);
  }
  if (isset($data['prix']) && $data['prix'] == "Payant"){
    $recherche = $recherche->where(function ($query) {
      $query->where("prix", ">", 0);
    });
  }
  $recherche = $recherche->get();
  $app->response()->headers->set('Content-Type', 'application/json');
  echo json_encode($recherche);

};

function creaEvent($app){
  $json = $app->request->getBody();
  $data = json_decode($json, true);
  $organisateur = Organisateur::where('email','=',$data['email'])->where('mdp','=',$data['pwd'])->count();
  if ($organisateur == 1){
    if(isset($data['nom_event']) && isset($data['adr_event']) && isset($data['date_debut']) && isset($data['date_fin']) && isset($data['nb_place']) && isset($data['desc_event']) && isset($data['photo_event'])
    && isset($data['prix_event']) && isset($data['cat_event']) && isset($data['nom_ville']) && isset($data['cp']) && isset($data['nom_dep'])){
      $organisateur = Organisateur::where('email','=',$data['email'])->where('mdp','=',$data['pwd'])->first();
      $ville = Ville::where('ville', 'LIKE', $data['nom_ville'])->get();
      if(strlen($ville)<3){
        $ville = new Ville;
        $ville->ville = $data['nom_ville'];
        $ville->departement = $data['nom_dep'];
        $ville->code_postal = $data['cp'];
        $ville->save();
        $ville = Ville::where('ville', 'LIKE', $data['nom_ville'])->get();
      }
      foreach ($ville as $key) {
        $id_ville = $key['id_ville'];
      }

      //On verifie que l'event n'existe pas déjà par rapport au nom, à l'organisateur et a l'adresse !
      $event = Event::where('intitule','LIKE',$data['nom_event'])->where('id_organisateur','LIKE',$organisateur['id_organisateur'])->where('adresse','LIKE',$data['adr_event'])->get();
      if (strlen($event)<3){
        //Traitement de l'upload image


        //Creation de l'event
        $event = new Event;
        $event->intitule = $data['nom_event'];
        $event->date_debut = $data['date_debut'];
        $event->date_fin = $data['date_fin'];
        $event->nb_place = $data['nb_place'];
        $event->adresse = $data['adr_event'];
        $event->description = $data['desc_event'];
        $event->photo = $data['photo_event'];
        $event->prix = $data['prix_event'];
        $event->id_organisateur = $organisateur['id_organisateur'];
        $event->id_categorie = $data['cat_event'];
        $event->id_ville = $id_ville;
        $event->save();
        $event = Event::where('intitule','LIKE',$data['nom_event'])->where('id_organisateur','LIKE',$organisateur['id_organisateur'])->where('adresse','LIKE',$data['adr_event'])->get();
        foreach ($event as $key => $value) {
          $key= $value['id_event'];
        }
        //Creation relation Niveau
        if(isset($data['niveau1']) && $data['niveau1'] == "true"){
          $niveau = new NiveauEvent;
          $niveau->id_event = $key;
          $niveau->id_niveau = 1;
          $niveau->save();
        }

        if(isset($data['niveau2']) && $data['niveau2'] == "true"){
          $niveau = new NiveauEvent;
          $niveau->id_event = $key;
          $niveau->id_niveau = 2;
          $niveau->save();
        }

        if(isset($data['niveau3']) && $data['niveau3'] == "true"){
          $niveau = new NiveauEvent;
          $niveau->id_event = $key;
          $niveau->id_niveau = 3;
          $niveau->save();
        }

        if(isset($data['niveau4']) && $data['niveau4'] == "true"){
          $niveau = new NiveauEvent;
          $niveau->id_event = $key;
          $niveau->id_niveau = 4;
          $niveau->save();
        }
        $app->response()->headers->set('Content-Type', 'application/json');
        $res = array("msg" =>"ok","key"=>$key);
        echo json_encode($res);
      }else{
        $app->response()->headers->set('Content-Type', 'application/json');
        echo json_encode("existe deja");
      }
    }
    else{
      $app->response()->headers->set('Content-Type', 'application/json');
      echo json_encode("manque info");
    }
  }else{

    $app->response()->headers->set('Content-Type', 'application/json');
    echo json_encode("mauvaise info co");
  }

};
?>
