<?php

use app\model\Categorie as Categorie;
use app\model\Niveau as Niveau;
use app\model\Event as Event;
use app\model\Ville as Ville;
use app\model\Participant as Participant;
use app\model\Participe as Participe;

include 'fonction.php';


//afficher les évents
$app->get('/liste', function () use ($app) {
  liste($app);
});

//Lister 6 derniers event
$app->get('/listeAccueil', function () use ($app) {
  listeAccueil($app);
});

//affichage event
$app->get('/event/:number', function($number) use ($app){
  event($app,$number);
});

//inscrit event
$app->get('/inscritEvent/:number', function($number) use ($app){
  inscritEvent($app,$number);
});

//nbinscrit event
$app->get('/nbInscritEvent/:number', function($number) use ($app){
  nbInscritEvent($app,$number);
});

//nbplace event
$app->get('/nbPlaceEvent/:number', function($number) use ($app){
  nbPlaceEvent($app,$number);
});

//inscriptionEvent
$app->post('/inscriptionEvent/:number', function($number) use ($app){
  inscriptionEvent($app,$number);
});

//desinscription
$app->delete('/desinscription/:number', function($number) use ($app){
  desinscription($app,$number);
});

//get dossard
$app->post('/getdossard/:number', function($number) use ($app){
  getdossard($app,$number);
});


//rechercheEvent
$app->post('/rechercheEvent', function () use ($app) {
  rechercheEvent($app);
});

//creation d'event
$app->post('/creaEvent', function() use ($app){
  creaEvent($app);
});

?>
